//
//  ViewController.swift
//  TextClassifier
//
//  Created by Pratik Thapa  on 10/11/18.
//  Copyright © 2018 Three Callistos. All rights reserved.
//

import UIKit
import NaturalLanguage
class ViewController: UIViewController {
  
  @IBOutlet weak var predicationLabel: UILabel!
  @IBOutlet weak var inputTextField: UITextField!
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  @IBAction func predictButtonPressed(_ sender: Any) {
    do {
      guard let txt = inputTextField.text else { return }
      let classPredictor = try NLModel(mlModel: WordClassifier().model)
//      let predication = try WordClassifier().prediction(text: txt.lowercased())
      predicationLabel.text = classPredictor.predictedLabel(for: txt.lowercased())
    } catch {
      print("error")
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
    super.touchesBegan(touches, with: event)
    view.endEditing(true)
    
  }
}

